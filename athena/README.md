Base Images for the Athena, AthSimulation and AtlasOffline Projects.
======================================

This procedure can be used to build an image providing a full
installation of an Athena, AthSimulation or AtlasOffline release.

Clone this repository
----
```bash
git clone https://gitlab.cern.ch/atlas-sit/docker.git
cd docker/athena
```

Building images with docker
----
Building an image for a numbered release in the Athena project:

```bash
docker build --network host -t atlas/athena:22.0.47 --build-arg BASEIMAGE=atlas/centos7-atlasos:latest-gcc11 --build-arg RELEASE=22.0.47 --build-arg PROJECT=Athena --build-arg BINARY_TAG=x86_64-centos7-gcc11-opt .
```

Building an image for a nightly release in the Athena project:

```bash
docker build --network host -t atlas/athena:22.0.59_2022-03-03T2101 --build-arg BASEIMAGE=atlas/centos7-atlasos:latest-gcc11 --build-arg RELEASE=22.0.59 --build-arg PROJECT=Athena --build-arg BINARY_TAG=x86_64-centos7-gcc11-opt --build-arg TIMESTAMP=2022-03-03T2101 .
```

Building an image for a nightly release in the AthSimulation project:

```bash
docker build --network host -t atlas/athsimulation:22.0.59 --build-arg BASEIMAGE=atlas/centos7-atlasos:latest-gcc11 --build-arg RELEASE=22.0.59 --build-arg PROJECT=AthSimulation --build-arg BINARY_TAG=x86_64-centos7-gcc11-opt --build-arg TIMESTAMP=2022-03-03T2101 .
```

Building an image for a numbered release in the AtlasOffline project:

```bash
docker build --network host -t atlas/athena:21.0.15 --build-arg BASEIMAGE=atlas/slc6-atlasos:latest --build-arg RELEASE=21.0.15 --build-arg PROJECT=AtlasOffline --build-arg BINARY_TAG=x86_64-slc6-gcc62-opt .
```

Available images at Docker Hub
--------
[Athena](https://hub.docker.com/repository/docker/atlas/athena)<br/> 
[AthSimulation](https://hub.docker.com/repository/docker/atlas/athsimulation)
