AthAnalysis-21.2.X Image Configuration
======================================

This configuration can be used to build an image providing a
completely standalone installation of an AthAnalysis-21.2.X release.

Building an image for the latest AthAnalysis release should be done
like:

```bash
docker build \
   -t atlas/athanalysis:21.2.83-centos7-20190802 \
   -t atlas/athanalysis:21.2.83-centos7 \
   -t atlas/athanalysis:21.2.83 \
   -t atlas/athanalysis:latest \
   --build-arg BASEIMAGE=atlas/centos7-atlasos:latest-gcc8 \
   --build-arg RELEASE=21.2.83 \
   --build-arg PLATFORM=x86_64-centos7-gcc8-opt \
   --compress --squash .
```

The base image for SLC6 releases should be
`BASEIMAGE=atlas/slc6-atlasos:latest`.

You should of course just leave off the "latest tag" from the command
line when building an image for an older release.

Examples
--------

You can find pre-built images on
[atlas/analysisbase](https://hub.docker.com/r/atlas/athanalysis/).
