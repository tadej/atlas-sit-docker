# CC7 OS capable of using/running the ATLAS software release(s).

# Make the base image configurable:
ARG BASEIMAGE=cern/cc7-base:latest

# Set up the CC7 "ATLAS OS":
FROM ${BASEIMAGE}

# Perform the installation as root:
USER root
WORKDIR /root

# Put the repository configuration file(s) in place:
COPY *.repo /etc/yum.repos.d/
# Copy the environment setup script(s) in place:
COPY atlas_prompt.sh cmake-3.24.3.sh /etc/profile.d/

# The LCG GCC package(s) to install as part of the image.
ARG GCCPACKAGES="gcc_11.2.0_x86_64_centos7 binutils_2.37_x86_64_centos7"
ARG GCCVERSION=11.2.0

# 1. Install extra CC7 and LCG packages needed by the ATLAS release
# 2. Install CMake
RUN yum -y install which git wget tar atlas-devel \
      redhat-lsb-core libX11-devel libXpm-devel libXft-devel libXext-devel \
      libaio libevent openssl-devel glibc-devel rpm-build nano sudo \
      ${GCCPACKAGES} \
    && yum clean all \
    && ln -s ${GCCVERSION}/x86_64-centos7 /opt/lcg/gcc/x86_64-centos7 \
    && wget https://cmake.org/files/v3.24/cmake-3.24.3-linux-x86_64.tar.gz \
    && mkdir -p /opt/cmake/3.24.3/Linux-x86_64 \
    && tar -C /opt/cmake/3.24.3/Linux-x86_64 --strip-components=1 \
      --no-same-owner -xvf cmake-*-linux-x86_64.tar.gz \
    && rm cmake-*-linux-x86_64.tar.gz

# Set up the ATLAS user, and give it super user rights.
RUN echo '%wheel	ALL=(ALL)	NOPASSWD: ALL' >> /etc/sudoers && \
    adduser atlas && chmod 777 /home/atlas && \
    usermod -aG wheel atlas && \
    usermod -aG root atlas && \
    mkdir /workdir && chown "atlas:atlas" /workdir && \
    chmod 777 /workdir

# LH: Make Images Grid / Singularity compatible
# Note that this is a hack as long as overlayfs
# is not widely on grid sites using singularity
# as a container runtime. For overlay-enabled
# runtimes (such as docker) missing mountpoints
# are created on demand. I hope we can remove this soon.
RUN mkdir -p /alrb /cvmfs /afs /eos

# Add basic usage instructions for the image:
COPY motd /etc/

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
