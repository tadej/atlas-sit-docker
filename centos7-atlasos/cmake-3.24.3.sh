# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Environment setup for CMake 3.24.3.
#

export PATH=/opt/cmake/3.24.3/Linux-x86_64/bin${PATH:+:${PATH}}
