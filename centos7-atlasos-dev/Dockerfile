# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# CentOS 7 based image allowing for full-fledged software development on the
# x86_64-centos7-gccX-opt platform.
#

# Make the base image configurable.
ARG BASEIMAGE=atlas/centos7-atlasos:latest

# Start from the aforementioned base image.
FROM ${BASEIMAGE}

# Switch to the root user for the image setup.
USER root
WORKDIR /root

# Install all the packages needed for ATLAS software development.
RUN yum -y install git wget atlas-devel ctags libX11-devel libXpm-devel \
      libXft-devel libXext-devel libXi-devel openssl-devel rpm-build   \
      gmp-devel mesa-libGL-devel mesa-libGLU-devel mesa-libEGL-devel \
      libcurl-devel glib2-devel xz-devel rpm-build openssl man-db \
    && yum clean all \
    && wget https://github.com/ninja-build/ninja/releases/download/v1.11.1/ninja-linux.zip \
    && mkdir -p /opt/ninja/1.11.1/Linux-x86_64 \
    && unzip ninja-linux.zip -d /opt/ninja/1.11.1/Linux-x86_64 \
    && chmod 755 /opt/ninja/*/Linux-x86_64/ninja \
    && rm ninja-linux.zip

# Put the environment setup scripts in place.
COPY etc/*.sh /etc/profile.d/

# Switch back to the ATLAS user.
USER atlas
WORKDIR /workdir

# Install the AtlasSetup configuration file.
COPY etc/.asetup /home/atlas/

# Start the image with BASH by default, after having printed the message
# of the day.
COPY etc/motd /etc/
CMD cat /etc/motd && /bin/bash
