# Image providing AnalysisBase/AnalysisTop-21.2.X for SLC6/CentOS7.

# Make the base image configurable.
ARG BASEIMAGE=atlas/slc6-atlasos:latest

# Set up the SLC6/CentOS7 "ATLAS OS".
FROM ${BASEIMAGE}

# Argument(s) that can be passed when building the image.
ARG PROJECT=AnalysisBase
ARG RELEASE=21.2.83
ARG PLATFORM=x86_64-slc6-gcc62-opt
ARG WORKDIR=/home/atlas

# Helper variable(s).
ARG ANALYSISRELEASE=${PROJECT}_${RELEASE}_${PLATFORM}

# Helper environment variables for the image.
ENV AtlasProject ${PROJECT}
ENV AtlasVersion ${RELEASE}

# Perform the installation as root.
USER root
WORKDIR /root

# 1. Install all "ayum packages"
# 2. Clean up to reduce the image size
RUN yum -y install \
      libpng \
      libjpeg-turbo \
      libtiff \
      ${ANALYSISRELEASE} && \
    yum clean all

# Remove the environment setup script(s) that are more trouble than they
# are worth.
RUN rm -f /etc/profile.d/color* /etc/profile.d/zzz_hepix.*

# Add the release setup script to the home directory, and its
# "usage instructions".
COPY release_setup.sh.in /
COPY motd.in /etc/
RUN sed "s/{{PROJECT}}/${PROJECT}/g" /release_setup.sh.in > \
    /release_setup.sh && \
    sed "s/{{PROJECT}}/${PROJECT}/g" /etc/motd.in > /etc/motd && \
    rm /release_setup.sh.in /etc/motd.in

# Switch back to the ATLAS account for the rest of the image
# setup.
USER atlas
WORKDIR ${WORKDIR}

# Set up a soft link for release_setup.sh inside the /home/atlas directory.
RUN ln -s /release_setup.sh /home/atlas/release_setup.sh

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
